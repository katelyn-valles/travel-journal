export default [
    {
        title: "Independence Hall",
        location: "PHILADELPHIA",
        googleMapsUrl: "https://www.google.com/maps/place/Independence+Hall,+520+Chestnut+St,+Philadelphia,+PA+19106/@39.9488737,-75.1500233,17z/data=!3m1!4b1!4m6!3m5!1s0x89c6c8836b121acd:0x4084dc5af8ba126a!8m2!3d39.9488737!4d-75.1500233!16zL20vMDFfeWg1?entry=ttu",
        startDate: "6 Oct, 2023",
        endDate: "14 Oct, 2023",
        description: "Named the birth place of America, Philadelphia was the meeting place for the 1st Continental Congress, the Constitutional Convention, and became the first capital of the United States.",
        imageUrl: "independence_hall.jpg"
    },
    {
        title: "Downtown Würzburg",
        location: "GERMANY",
        googleMapsUrl: "https://www.google.com/maps/place/W%C3%BCrzburg,+Germany/@49.7780439,9.8606291,12z/data=!3m1!4b1!4m6!3m5!1s0x47a2902012da4dd9:0x41db728f06209a0!8m2!3d49.7913044!4d9.9533548!16zL20vMDFwcWZj?entry=ttu",
        startDate: "1 June, 2022",
        endDate: "30 June, 2023",
        description: "Würzburg is a city in the region of Franconia, which is in the northern tip of Bavaria, Germany. Rich in history, Würzburg dates back to a Celtic fortification established around the 4th century. One of the city's most significant landmarks is the Würzburg Residence, a palatial building completed in the 18th century and now a UNESCO World Heritage Site. ",
        imageUrl: "wurzburg.jpg"
    },
    {
        title: "Key West",
        location: "FLORIDA",
        googleMapsUrl: "https://www.google.com/maps/place/Key+West,+FL+33040/@24.5652207,-81.8555671,12z/data=!3m1!4b1!4m6!3m5!1s0x88d1b134ad952377:0x3fcee92f77463b5e!8m2!3d24.5550593!4d-81.7799871!16zL20vMHJwNDY?entry=ttu",
        startDate: "30 Dec, 2019",
        endDate: "6 Jan, 2020",
        description: "The Overseas Highway, also known as U.S. Highway 1, runs from Miami to Key West and it one of the most scenic drives in the U.S., featuring 42 bridges over the waters of the Atlantic Ocean and Gulf of Mexico.",
        imageUrl: "keywest.jpg"
    }

    
]