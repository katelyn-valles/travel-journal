import React from "react"
import Navbar from "./components/Navbar"
import TravelItem from "./components/TravelItem"
import travelData from "./data"

function App(){
    
    const travelItems = travelData.map(item =>{
        return (
            <TravelItem
                key={item.id}
                item={item}
            />
        )
    })
    
    
    return(
      <div className="travel-container">
        <Navbar />
        <section className="travel-list">
          {travelItems}
        </section>
      </div>
    )
}

export default App;