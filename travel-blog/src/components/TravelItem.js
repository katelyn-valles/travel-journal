import React from "react"

function TravelItem(props)
{
    console.log(props);
    return(
        <div className ="travel-item-content">
        <img src={`/images/${props.item.imageUrl}`} className="travel-image" alt="destination" />
            <div className="travel-info">
                <div className="item-location-maplink">
                <span className="item-location">{props.item.location}</span>
                <a href={props.item.googleMapsUrl} className="item-maplink">View on Google Maps</a>
            </div>
                <h2 className="item-title">{props.item.title}</h2>
                <p className="travel-dates">{props.item.startDate} - {props.item.endDate}</p>
                <span className="travel-description">{props.item.description}</span> 
                
            </div>
            
        </div>
 
    )
}

export default TravelItem;